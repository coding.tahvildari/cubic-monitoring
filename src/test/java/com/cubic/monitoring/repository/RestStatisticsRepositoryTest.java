package com.cubic.monitoring.repository;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@DataJpaTest
class RestStatisticsRepositoryTest {

    @Autowired
    RestStatisticsRepository restStatisticsRepository;
    @Test
    void findFirst15ByOrderByIdDesc() {
        restStatisticsRepository.findFirst15ByOrderByIdDesc();
    }

    @Test
    void getRestInfoPerMinute() {
        restStatisticsRepository.getRestInfoPerMinute();
    }

    @Test
    void getRestInfoPerHour() {
        restStatisticsRepository.getRestInfoPerHour();
    }
}
package com.cubic.monitoring.controller;

import com.cubic.monitoring.domain.RestStatisticsModel;
import com.cubic.monitoring.domain.custom.CustomizedQueryResult;
import com.cubic.monitoring.repository.RestStatisticsRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = ChartController.class)
class ChartControllerTest {

    @MockBean
    RestStatisticsRepository restStatisticsRepository;
    RestStatisticsModel model;
    CustomizedQueryResult customizedQueryResult;
    @Autowired
    private MockMvc mockMvc;

    @BeforeEach

    void setUp() {
        model = new RestStatisticsModel("1", 5L, 15L, 10L, 2L, 123456789L);

        customizedQueryResult = new CustomizedQueryResult() {
            @Override
            public long getAverageTime() {
                return 10;
            }

            @Override
            public long getMinTime() {
                return 5;
            }

            @Override
            public long getMaxTime() {
                return 15;
            }

            @Override
            public String getFormattedDateTime() {
                return "2020-08-10 11:33:42.185";
            }
        };
    }

    @Test
    void getLastTenSecondsData() throws Exception {


        model.setCreationDate(new Date());
        List<RestStatisticsModel> modelList = new ArrayList<>();
        modelList.add(model);
        given(restStatisticsRepository.findFirst15ByOrderByIdDesc()).willReturn(modelList);
        mockMvc.perform(post("/rest/statistics/tenseconds")
                .contentType("application/json"))
                .andExpect(status().isOk()).andExpect(jsonPath("min[0]", is(5)))
                .andExpect(jsonPath("max[0]", is(15))).andExpect(jsonPath("averages[0]", is(10)));
    }

    @Test
    void getLastMinuteData() throws Exception {
        List<CustomizedQueryResult> modelList = new ArrayList<>();
        modelList.add(customizedQueryResult);
        given(restStatisticsRepository.getRestInfoPerMinute()).willReturn(modelList);
        mockMvc.perform(post("/rest/statistics/lastminute")
                .contentType("application/json"))
                .andExpect(status().isOk()).andExpect(jsonPath("min[0]", is(5)))
                .andExpect(jsonPath("max[0]", is(15))).andExpect(jsonPath("averages[0]", is(10)));
    }

    @Test
    void getLastHourData() throws Exception {
        List<CustomizedQueryResult> modelList = new ArrayList<>();
        modelList.add(customizedQueryResult);
        given(restStatisticsRepository.getRestInfoPerHour()).willReturn(modelList);
        mockMvc.perform(post("/rest/statistics/lasthour")
                .contentType("application/json"))
                .andExpect(status().isOk()).andExpect(jsonPath("min[0]", is(5)))
                .andExpect(jsonPath("max[0]", is(15))).andExpect(jsonPath("averages[0]", is(10)));
    }
}
package com.cubic.monitoring.service;

import com.cubic.monitoring.domain.RestInfo;
import org.springframework.scheduling.annotation.Async;

public interface RestInfoService {
    /**
     * Insert Rest Web Web Service Call Information by the RestInfo Class
     * @param restInfo
     */
    void addRestInfo(RestInfo restInfo);
}

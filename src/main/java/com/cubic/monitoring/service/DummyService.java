package com.cubic.monitoring.service;

import org.springframework.http.ResponseEntity;

public interface DummyService {
//DummyRest web service call
    ResponseEntity<String> getDummyRest();
}

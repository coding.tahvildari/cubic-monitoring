package com.cubic.monitoring.service;

import com.cubic.monitoring.domain.RestInfo;
import com.cubic.monitoring.domain.RestStatistics;

public interface RestStatisticsService {


    void recordRestInfo(RestInfo restInfo);

    /**
     * Calculate new seconds information and RestCache updates at the last second of every 10 seconds
     */
     void SaveStatisticsAndClearMap();
}

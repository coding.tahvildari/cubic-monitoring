package com.cubic.monitoring.service.impl;

import com.cubic.monitoring.service.DummyService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * This class is for calling DummyRest web service
 */
@Service
public class DummyServiceImpl implements DummyService {
    @Value("${dummyRest.uri}")
    String uri;


    @Override
    public ResponseEntity<String> getDummyRest() {

        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.getForEntity(uri, String.class);

    }
}

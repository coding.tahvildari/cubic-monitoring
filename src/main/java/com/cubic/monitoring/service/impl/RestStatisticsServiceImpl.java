package com.cubic.monitoring.service.impl;

import com.cubic.monitoring.cache.RestInfoCache;
import com.cubic.monitoring.domain.RestInfo;
import com.cubic.monitoring.domain.RestStatistics;
import com.cubic.monitoring.domain.RestStatisticsModel;
import com.cubic.monitoring.repository.RestStatisticsRepository;
import com.cubic.monitoring.service.RestStatisticsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.UUID;
import java.util.stream.IntStream;

/**
 * In this class, information about the Rest's web service call time is calculated per second and recorded in the RestCache class,
 * and a total of the last 10 second call information is stored. Also, every second, the relevant information of the latest second
 * will replace the last second
 */
@Service
public class RestStatisticsServiceImpl implements RestStatisticsService {


    private final RestInfoCache restCache;
    RestStatisticsRepository repository;

    @Autowired
    public RestStatisticsServiceImpl(RestInfoCache restCache,
                                     RestStatisticsRepository repository) {
        this.restCache = restCache;
        this.repository = repository;
        IntStream.range(0, 9).forEach((second) -> restCache.getRestStatisticsMap().put(second, new RestStatistics()));
    }

    @Override
    public void recordRestInfo(RestInfo restInfo) {
        int second = LocalDateTime
                .ofInstant(Instant.ofEpochMilli(restInfo.getCreatedTime()), ZoneId.systemDefault())
                .getSecond() % 10;

        restCache.getRestStatisticsMap().
                computeIfPresent(second, (key, restStatistics) ->
                {
                    restStatistics.addRestInfo(restInfo);
                    return restStatistics;
                });

    }


    @Override
    public void SaveStatisticsAndClearMap() {
        RestStatistics statistics = restCache.getRestStatisticsMap().values().stream().map(RestStatistics::new)
                .reduce(new RestStatistics(), (s1, s2) -> {
                    s1.setTotalResponseTime(s1.getTotalResponseTime() + s2.getTotalResponseTime());
                    s1.setCount(s1.getCount() + s2.getCount());
                    s1.setMaxResponseTime(Math.max(s1.getMaxResponseTime(), s2.getMaxResponseTime()));
                    s1.setMinResponseTime(Math.min(s1.getMinResponseTime(), s2.getMinResponseTime()));
                    s1.setLastResponse(Math.max(s1.getLastResponse(), s2.getLastResponse()));
                    return s1;
                });
        repository.save(new RestStatisticsModel(UUID.randomUUID().toString(),
                statistics.getMinResponseTime(),
                statistics.getMaxResponseTime(),
                statistics.getCount() == 0 ? 0 : statistics.getTotalResponseTime() / statistics.getCount(),
                statistics.getCount(),
                statistics.getLastResponse()

        ));

        restCache.getRestStatisticsMap().replaceAll((k, v) -> new RestStatistics());

    }

}



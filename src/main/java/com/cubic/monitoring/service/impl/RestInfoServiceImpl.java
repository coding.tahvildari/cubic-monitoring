package com.cubic.monitoring.service.impl;

import com.cubic.monitoring.domain.RestInfo;
import com.cubic.monitoring.service.RestInfoService;
import com.cubic.monitoring.service.RestStatisticsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * The job of this class is to record data related to RestInfo
 */
@Service
public class RestInfoServiceImpl implements RestInfoService {

    RestStatisticsService restStatisticsService;

    @Autowired
    public RestInfoServiceImpl(com.cubic.monitoring.service.RestStatisticsService restStatisticsService) {
        this.restStatisticsService = restStatisticsService;
    }

    @Override
    public void addRestInfo(RestInfo restInfo) {
        restStatisticsService.recordRestInfo(restInfo);
    }
}

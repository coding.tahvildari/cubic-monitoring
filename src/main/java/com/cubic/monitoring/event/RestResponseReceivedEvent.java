package com.cubic.monitoring.event;

import com.cubic.monitoring.domain.RestInfo;
import lombok.Value;

/**
 * Incident related to calling the new Rest web service
 */
@Value
public class RestResponseReceivedEvent {
    RestInfo restInfo;
}

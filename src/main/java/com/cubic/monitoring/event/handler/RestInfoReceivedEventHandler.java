package com.cubic.monitoring.event.handler;

import com.cubic.monitoring.event.RestResponseReceivedEvent;
import com.cubic.monitoring.service.RestInfoService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import java.time.Instant;

@Component
public class RestInfoReceivedEventHandler {
    @Value("${clearCacheJob.acceptedPeriod}")
    long acceptedPeriod;
    RestInfoService restInfoService;

    public RestInfoReceivedEventHandler(RestInfoService restInfoService) {
        this.restInfoService = restInfoService;
    }

    /**
     * Detects the RestInfo web calling event and follows the process
     * The call is acceptable if it is related to the last 10 seconds
     *
     * @param receivedEvent
     */
    @Async
    @EventListener
    public void transactionReceived(final RestResponseReceivedEvent receivedEvent) {
        if (Instant.ofEpochMilli(receivedEvent.getRestInfo().getCreatedTime())
                .isAfter(Instant.now().minusSeconds(acceptedPeriod))) {
            restInfoService.addRestInfo(receivedEvent.getRestInfo());
        }

    }
}

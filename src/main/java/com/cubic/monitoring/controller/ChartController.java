package com.cubic.monitoring.controller;

import com.cubic.monitoring.domain.RestStatisticsModel;
import com.cubic.monitoring.domain.custom.CustomizedQueryResult;
import com.cubic.monitoring.repository.RestStatisticsRepository;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("/rest/statistics")
public class ChartController {

    @Autowired
    RestStatisticsRepository statisticsRepository;

    /**
     * @return This method takes information about the maximum, minimum, average response time of Rest call in the last ten seconds
     * of the database and sends it in the form of Json string for display in monitoring..
     */
    @RequestMapping("/tenseconds")
    @ResponseBody
    public String getLastTenSecondsData() {

        List<RestStatisticsModel> statisticsModelList = statisticsRepository.findFirst15ByOrderByIdDesc();
        Collections.sort(statisticsModelList);
        JsonArray jsonCreatedDate = new JsonArray();
        JsonArray jsonAvgTime = new JsonArray();
        JsonArray jsonMaxTime = new JsonArray();
        JsonArray jsonMinTime = new JsonArray();
        JsonObject jsonObject = new JsonObject();
        statisticsModelList.forEach(data -> {
            jsonCreatedDate.add(data.getCreationDate().toString());
            jsonAvgTime.add(data.getAvg());
            jsonMaxTime.add(data.getMax());
            jsonMinTime.add(data.getMin());

        });
        jsonObject.add("times", jsonCreatedDate);
        jsonObject.add("averages", jsonAvgTime);
        jsonObject.add("max", jsonMaxTime);
        jsonObject.add("min", jsonMinTime);
        return jsonObject.toString();
    }

    /**
     * @return This method takes information about the maximum, minimum, average response time of Rest call in the recent minute
     * of the database and sends it in the form of Json string for display in monitoring..
     */
    @RequestMapping("/lastminute")
    @ResponseBody
    public String getLastMinuteData() {

        List<CustomizedQueryResult> restInfoPerMinute = statisticsRepository.getRestInfoPerMinute();
        JsonArray jsonCreatedDate = new JsonArray();
        JsonArray jsonAvgTime = new JsonArray();
        JsonArray jsonMaxTime = new JsonArray();
        JsonArray jsonMinTime = new JsonArray();
        JsonObject jsonObject = new JsonObject();
        restInfoPerMinute.forEach(data -> {
            jsonCreatedDate.add(data.getFormattedDateTime());
            jsonAvgTime.add(data.getAverageTime());
            jsonMaxTime.add(data.getMaxTime());
            jsonMinTime.add(data.getMinTime());

        });
        jsonObject.add("times", jsonCreatedDate);
        jsonObject.add("averages", jsonAvgTime);
        jsonObject.add("max", jsonMaxTime);
        jsonObject.add("min", jsonMinTime);
        return jsonObject.toString();
    }

    /**
     * @return This method takes information about the maximum, minimum, average response time of Rest call in the recent hour
     * of the database and sends it in the form of Json string for display in monitoring..
     */
    @RequestMapping("/lasthour")
    @ResponseBody
    public String getLastHourData() {

        List<CustomizedQueryResult> restInfoPerMinute = statisticsRepository.getRestInfoPerHour();
        JsonArray jsonCreatedDate = new JsonArray();
        JsonArray jsonAvgTime = new JsonArray();
        JsonArray jsonMaxTime = new JsonArray();
        JsonArray jsonMinTime = new JsonArray();
        JsonObject jsonObject = new JsonObject();
        restInfoPerMinute.forEach(data -> {
            jsonCreatedDate.add(data.getFormattedDateTime());
            jsonAvgTime.add(data.getAverageTime());
            jsonMaxTime.add(data.getMaxTime());
            jsonMinTime.add(data.getMinTime());

        });
        jsonObject.add("times", jsonCreatedDate);
        jsonObject.add("averages", jsonAvgTime);
        jsonObject.add("max", jsonMaxTime);
        jsonObject.add("min", jsonMinTime);
        return jsonObject.toString();
    }


}
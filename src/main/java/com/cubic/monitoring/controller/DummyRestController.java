package com.cubic.monitoring.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/cubic/rest")
public class DummyRestController {

    private final Logger log = LoggerFactory.getLogger(DummyRestController.class);

    @Value("${dummyRest.responseDelay}")

    /*
     * This is a simple rest web service that is called by the program at configurable time and its response time is calculated
     * and displayed on the monitor.
     */
    @GetMapping("/dummy")
    @ResponseBody
    public ResponseEntity<String> delayResponse() {

        return ResponseEntity.ok("Hello World!");
    }

}



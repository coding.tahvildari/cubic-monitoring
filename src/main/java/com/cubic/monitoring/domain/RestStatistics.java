package com.cubic.monitoring.domain;

import com.sun.istack.NotNull;
import lombok.Data;

import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * This class stores information about different Rest Call modes, every second a new instance of this class replaces the previous
 * instance in RestInfoCache. To create ThreadSafety mode, the Locking process is used when adding a new instance of the RestInfo class.
 */
@Data
public class RestStatistics {
    private ReentrantReadWriteLock lock = new ReentrantReadWriteLock(true);
    @NotNull
    private long totalResponseTime;
    @NotNull
    private long minResponseTime;
    @NotNull
    private long avgResponseTime;
    @NotNull
    private long maxResponseTime;
    @NotNull
    private long count;
    @NotNull
    private long lastResponse;

    public RestStatistics() {
        this.minResponseTime = Long.MAX_VALUE;
        this.avgResponseTime = 0;
        this.maxResponseTime = 0;
        this.count = 0;
    }

    public RestStatistics(RestStatistics restStatistics) {
        this.totalResponseTime = restStatistics.getTotalResponseTime();
        this.minResponseTime = restStatistics.getMinResponseTime();
        this.maxResponseTime = restStatistics.getMaxResponseTime();
        this.count = restStatistics.getCount();
        this.lastResponse = restStatistics.getLastResponse();

    }


    public void addRestInfo(RestInfo restInfo) {
        lock.writeLock().lock();
        minResponseTime = count == 0 ? restInfo.getResponseTime() : Math.min(minResponseTime, restInfo.getResponseTime());
        maxResponseTime = Math.max(maxResponseTime, restInfo.getResponseTime());
        count++;
        lastResponse = System.currentTimeMillis();
        totalResponseTime += restInfo.getResponseTime();
        avgResponseTime = totalResponseTime / count;
        lock.writeLock().unlock();
    }
}

package com.cubic.monitoring.domain;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Information about response time and rest execution time is stored and sent by this class
 */
@Data
@AllArgsConstructor
public class RestInfo {
    @NotNull
    private long responseTime;

    @NotNull
    private long createdTime;

}

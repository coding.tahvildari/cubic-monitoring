package com.cubic.monitoring.domain.custom;

/**
 * From this class to display Rest call information in charts of
 * + Show every minute
 * + Show every hour
 * Used
 */
public interface CustomizedQueryResult {

    long getAverageTime();

    long getMinTime();

    long getMaxTime();

    String getFormattedDateTime();


}

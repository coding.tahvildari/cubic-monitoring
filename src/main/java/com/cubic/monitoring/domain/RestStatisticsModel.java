package com.cubic.monitoring.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.*;

/**
 * A model that lists information about the mean and maximum and minimum response times of the last 10 seconds.
 */
@Entity
@Table(name = "rest_statistics")
@Data
@AllArgsConstructor
public class RestStatisticsModel extends Auditable<String> implements Comparable<RestStatisticsModel>{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String id;

    @Column(name = "MIN_RESPONSE_TIME")
    private Long min;

    @Column(name = "MAX_RESPONSE_TIME")
    private Long max;

    @Column(name = "AVERAGE_RESPONSE_TIME")
    private Long avg;

    @Column(name = "REST_COUNTS")
    private Long count;

    @Column(name = "LAST_REST_CALL_TIME")
    private Long lastResponse;

    public RestStatisticsModel() {
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RestStatisticsModel)) {
            return false;
        }
        return id != null && id.equals(((RestStatisticsModel) o).id);
    }

    @Override
    public String toString() {
        return "RestStatistics{" +
                "id=" + getId() +
                ", min=" + getMin() +
                ", max=" + getMax() +
                ", avg=" + getAvg() +
                ", count=" + getCount() +
                ", lastResponse='" + getLastResponse() + "'" +
                "}";
    }



    @Override
    public int compareTo(RestStatisticsModel o) {
        return super.creationDate.compareTo(o.getCreationDate());
    }
}

package com.cubic.monitoring.aop.rest;


import com.cubic.monitoring.domain.RestInfo;
import com.cubic.monitoring.event.RestResponseReceivedEvent;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Configuration;


/* Aspect for measuring the execution of a DummyRest
 *When a call is made from the Rest web service, AOP records the start and end time of the web service call and
 * records the service response time. This event is published in the program by Spring Event and the total
 * response time calculation process is recorded. It is done in the database and its display in monitoring
 */
@Aspect
@Configuration
public class RestCallAspect {


    private final ApplicationEventPublisher eventPublisher;

    @Autowired
    public RestCallAspect(ApplicationEventPublisher eventPublisher) {
        this.eventPublisher = eventPublisher;
    }


    @Around("within(com.cubic.monitoring.service.impl.DummyServiceImpl)")
    public void around(ProceedingJoinPoint joinPoint) throws Throwable {
        long startTime = System.currentTimeMillis();
        joinPoint.proceed();
        long timeTaken = System.currentTimeMillis() - startTime;
        eventPublisher.publishEvent(new RestResponseReceivedEvent(new RestInfo(timeTaken, startTime)));
    }
}

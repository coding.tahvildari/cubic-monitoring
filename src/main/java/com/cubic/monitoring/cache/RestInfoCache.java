package com.cubic.monitoring.cache;

import com.cubic.monitoring.domain.RestStatistics;
import lombok.Getter;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/*
 * ConcurrentHashMap data structure is used to persist the rest calls.Retains data on the maximum, minimum, and average, and number of
 * DummyRest web service calls per second aggregated by the RestStatistics object. This information is stored in the database
 * every 10 seconds.Also, the information of this map is updated every second and the information of calling web services
 *  for more than ten seconds is emptied and deleted by a job.
 */


@Component
@Getter
public class RestInfoCache {

    private final Map<Integer, RestStatistics> restStatisticsMap
            = new ConcurrentHashMap<>();

}

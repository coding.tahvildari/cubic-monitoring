package com.cubic.monitoring.repository;

import com.cubic.monitoring.domain.custom.CustomizedQueryResult;
import com.cubic.monitoring.domain.RestStatisticsModel;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RestStatisticsRepository extends CrudRepository<RestStatisticsModel, Long> {

    List<RestStatisticsModel> findFirst15ByOrderByIdDesc();

    @Query(value = "SELECT  CAST( (SUM(AVERAGE_RESPONSE_TIME)/ COUNT (*))    AS DECIMAL(18,0) ) as averageTime \n" +
            "         ,  MIN(MIN_RESPONSE_TIME)  as minTime, MAX(MAX_RESPONSE_TIME) as maxTime,  FORMATDATETIME(CREATION_DATE,'YYYY-DD-MM HH-mm')   " +
            "AS formattedDateTime FROM REST_STATISTICS GROUP BY  formattedDateTime   ORDER BY formattedDateTime  DESC  limit 10" ,
    nativeQuery = true)
    List<CustomizedQueryResult> getRestInfoPerMinute();

    @Query(value = "SELECT  CAST( (SUM(AVERAGE_RESPONSE_TIME)/ COUNT (*))    AS DECIMAL(18,0) ) as averageTime \n" +
            "         ,  MIN(MIN_RESPONSE_TIME)  as minTime, MAX(MAX_RESPONSE_TIME) as maxTime,  FORMATDATETIME(CREATION_DATE,'YYYY-DD-MM HH')   " +
            "AS formattedDateTime FROM REST_STATISTICS GROUP BY  formattedDateTime   ORDER BY formattedDateTime  DESC  limit 10" ,
            nativeQuery = true)
    List<CustomizedQueryResult> getRestInfoPerHour();
}

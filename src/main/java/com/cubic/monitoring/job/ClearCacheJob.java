package com.cubic.monitoring.job;

import com.cubic.monitoring.event.RestResponseReceivedEvent;
import com.cubic.monitoring.service.RestInfoService;
import com.cubic.monitoring.service.RestStatisticsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.Instant;

/**
 *
 */
@Component
public class ClearCacheJob {

    RestStatisticsService restStatisticsService;

    @Autowired
    public ClearCacheJob(RestStatisticsService restStatisticsService) {
        this.restStatisticsService = restStatisticsService;
    }


    /**
     * This job updates every 1 second the information stored in the last 1 second
     * (out of a total of 10 seconds saved) in RestCache
     */
    @Scheduled(fixedDelayString = "${clearRestStatisticsCacheJob.fixedDelay}",
            initialDelayString = "${clearRestStatisticsCacheJob.initialDelay}")
    public void clearRestStatisticsCache() {
        restStatisticsService.SaveStatisticsAndClearMap();

    }


}

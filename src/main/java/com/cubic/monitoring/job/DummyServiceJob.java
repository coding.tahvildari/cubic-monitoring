package com.cubic.monitoring.job;

import com.cubic.monitoring.service.DummyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * At customizable intervals, the web service calls DummyRest for display on the monitor
 */
@Component
public class DummyServiceJob {

    DummyService dummyService;

    @Autowired
    public DummyServiceJob(DummyService dummyService) {
        this.dummyService = dummyService;
    }


    @Scheduled(fixedDelayString = "${dummyRestJob.fixedDelay}", initialDelayString = "${dummyRestJob.initialDelay}")
    public void doSomething() {
        ResponseEntity<String> responseEntity = dummyService.getDummyRest();
    }
}

setInterval(ajaxCall, 15000);

function ajaxCall() {
    $.ajax({
        url: "rest/statistics/lastminute",
        success: function (result) {
            var times =  null;
            times = JSON.parse(result).times;
            times = times.map(getMinite);
            var averages = null;
            averages = JSON.parse(result).averages;
            var max = null;
            max = JSON.parse(result).max;
            var min = null;
            min = JSON.parse(result).min;

            drawMinuteChart(times.reverse(),
                averages.reverse(),
                max.reverse(),
                min.reverse());
        }
    });
}

function drawMinuteChart(times, avgs, max, min) {
    Highcharts.chart('minuteChart', {
        chart: {
            type: 'line',
            width: 300
        },

        title: {
            text: 'Average response time per minute'
        },
        subtitle: {
            text: 'Average-Highest-Lowest'
        },

        yAxis: {
            title: {
                text: 'Millisecond'
            }
        },

        xAxis: {

            categories: times
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle'
        },

        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: false
            }
        },
        responsive: {
            rules: [{
                condition: {
                    maxWidth: 500
                },
                chartOptions: {
                    legend: {
                        layout: 'horizontal',
                        align: 'center',
                        verticalAlign: 'bottom'
                    }
                }
            }]
        },


        series: [{
            name: 'Average',
            data: avgs
        },
            {
                name: 'max',
                data: max
            },
            {
                name: 'min',
                data: min
            }
        ]
    });
}


function getMinite(timeStamp) {
    return timeStamp.substring(11, 17);
}


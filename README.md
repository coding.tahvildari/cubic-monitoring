# Cubic Monitoring
This monitoring system Send every (configurable) ms an http (s) request against url (configurable).
And these response times are based on the maximum, minimum, average response time entered in the system and in the charts
* Average Response Time Per 10 Seconds
* Average Response Time Per Hour
* Average Response Time Per Minute

is shown

The ability to download information is also available in the following formats
CSV, XLS, Data Table, PNG, JPEG, PDF



#Averaged the values, Thread safe , Performance
Three  important points in this monitoring are ThreadSafety and Performance, Averaged the values


For ThreadSafety, the Lock, Unlock mechanism is used when updating the summary information entered in the last 1 second. The last 10 second response time information is also entered in the ConcurrentHashMap as a caching mechanism.

ّFor Averaged the values, The Aspect Orient Programminf mechanism is used to calculate the sum. When the Rest Web service is called, the AOP publishes runtime and response time information as an event. This event is then received and the entire web services response time is counted as Thread Safe in one second, and finally the data per second is stored in a ConcurrentHashMap. All 10 of these data are summarized and entered in the table. Aggregation on the database side has also been used to calculate myagins per minute and hour.

For Performance, This method has also been used to create performance and insert the minimum number of records in the database. First, the information about the received response times during each second is stored in the RestStatistics class as Thread Safe using the Locking mechanism, and then after the completion of each second, this information is stored in ConcurrentHashMap. 
After 10 seconds, all data (maximum, lowest, average) response time (for the last 10 seconds) is entered in the table, which reduces the number of records entered in the table. Also, to reduce the load on the application side, the information related to the calculation of Rest call data in the last 10 minutes and 10 hours is done on the database side and the related information is received by the server.

##Time & space complexities
All operations calculate averages and save in Cache and display in monitoring to have time and space complexities of O(1)
All calculations are done taking into account Map Statistic and the output to get the status of the response time of the last 10 seconds is based on the Statistics entered in the Map.
Considering that the overdue calculations are delayed after 10 seconds and new calculations replace them.
which means method runs with O (1) complexity


###H2 Database console
While the application is running, you can access the H2 in memory database console. To access the H2 console, navigate to http://localhost:8080/h2-console and enter the following value in the URL field: jdbc:h2:mem:testdb       user= sa   ,  password= password


##Technology used
Spring boot : Web application

Spring Data: Dao Layer

Maven : Building project

Java 11 : programming language

Swagger : RESTful Documentation

Jquery, Highcharts : Show charts


###Instructions to build

###Prerequisite

Maven
Java 11 or higher
mvn clean package
###Start the application
######Using generated executable jar
java -jar target\monitoring-0.0.1-SNAPSHOT.jar

####The apis are available on http://<server-name/ip>:8080

#####Swagger is already integrated and can be used for using API instead of curl.

http://localhost:8080/swagger-ui.html

